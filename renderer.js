// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

let App = require('./app/App');

let appElement = document.querySelector('app');

if(appElement) {
	m.mount(appElement, App);
}
