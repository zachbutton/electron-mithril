const COLOR_1 = '#00ff00';
const COLOR_2 = '#ff0000';

let ColorBox = {
	view(vnode) {
		return (
			<div class="color-box" style={{ background: vnode.attrs.color }}>
				{ vnode.attrs.color }
			</div>
		);
	}
};

let App = {
	oninit(vnode) {
		this.color = COLOR_1;
	},
	toggle() {
		if(this.color == COLOR_1) {
			this.color = COLOR_2;
		} else {
			this.color = COLOR_1;
		}
	},
	view(vnode) {
		return (
			<div>
				<div class="highlighted-box">
					<h1>Hard coded as black</h1>
					<ColorBox color="black" />
				</div>

				<div class="highlighted-box">
					<h1>Variable color</h1>
					<ColorBox color={ this.color } />
				</div>

				{ this.color == COLOR_1 || this.color == COLOR_2 ? (
					<h1>Probably toggling</h1>
				) : (
					<h1>Probably <b>NOT</b> toggling</h1>
				) }
				
				<button onclick={ () => this.toggle() }>Toggle Color</button>

				<div>
					<input oninput={ e => this.color = e.target.value } value={ this.color } />
				</div>
			</div>
		);
	}
};

module.exports = App;
